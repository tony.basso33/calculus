/**
 * Calculate average of everything
 * @param {Array} data 
 */
 module.exports = function calculate(data){
    
    let averages = 0;
    let subjectNumber = 0;
    let generalAverage = 0;

    //group average
    data.forEach((group) => 
    {
        if(group.coef != 0)
        {
            subAverages = [];
            
            group.subjects.forEach((subject) => {
                //subject average
                if(subject.coef != 0)
                    subAverages.push(sumSubject(subject.notes, subject.coef));
            });
    
            //get sum of evey subject in list
            let res = sumGroup(subAverages, group.coef);
            if(!isNaN(res))
            {
                averages += res;
                subjectNumber++;
            }
        }
    });

    //groups weighting
    generalAverage = averages/subjectNumber;


    console.log("General average: " + generalAverage.toFixed(2));
    return generalAverage.toFixed(2);
}


function sumSubject(notes, coef) {
    subAvg = 0;
    subCoefs = 0;

    //notes sum
    notes.forEach((note) => {
        subAvg += note.value * note.coef;
        subCoefs += note.coef;
    });

    //weighting
    subAvg = subAvg/subCoefs;
    return subAvg;
}

function sumGroup (averages, coef) {       
    groupAvg = 0;
    groupCoefs = 0;

    //subjects averages sum
    averages.forEach((avg) =>{
        groupAvg += avg * coef;
        groupCoefs += coef;
    });
                
    //weighting
    groupAvg = groupAvg/groupCoefs;
    return groupAvg;
}