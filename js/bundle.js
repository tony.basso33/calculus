(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/**
 * Calculate average of everything
 * @param {Array} data 
 */
 module.exports = function calculate(data){
    
    let averages = 0;
    let subjectNumber = 0;
    let generalAverage = 0;

    //group average
    data.forEach((group) => 
    {
        if(group.coef != 0)
        {
            subAverages = [];
            
            group.subjects.forEach((subject) => {
                //subject average
                if(subject.coef != 0)
                    subAverages.push(sumSubject(subject.notes, subject.coef));
            });
    
            //get sum of evey subject in list
            let res = sumGroup(subAverages, group.coef);
            if(!isNaN(res))
            {
                averages += res;
                subjectNumber++;
            }
        }
    });

    //groups weighting
    generalAverage = averages/subjectNumber;


    console.log("General average: " + generalAverage.toFixed(2));
    return generalAverage.toFixed(2);
}


function sumSubject(notes, coef) {
    subAvg = 0;
    subCoefs = 0;

    //notes sum
    notes.forEach((note) => {
        subAvg += note.value * note.coef;
        subCoefs += note.coef;
    });

    //weighting
    subAvg = subAvg/subCoefs;
    return subAvg;
}

function sumGroup (averages, coef) {       
    groupAvg = 0;
    groupCoefs = 0;

    //subjects averages sum
    averages.forEach((avg) =>{
        groupAvg += avg * coef;
        groupCoefs += coef;
    });
                
    //weighting
    groupAvg = groupAvg/groupCoefs;
    return groupAvg;
}
},{}],2:[function(require,module,exports){
class Subject {
    name = "";
    coef = 0;
    notes = new Array();

    constructor(name, coef, notes) {
        this.name = name;
        this.coef = coef;
        this.notes = notes;
    }
}

class Group {
    name = "";
    coef = 0;
    subjects = new Array();

    constructor(name, coef, subjects) {
        this.name = name;
        this.coef = coef;
        this.subjects = subjects;
    }
}

class Note {
    name = "";
    coef = 0;
    value = 0;

    constructor(name, coef, value) {
        this.name = name;
        this.coef = coef;
        this.value = value;
    }
}

module.exports = {
    Subject: Subject,
    Group: Group,
    Note: Note,
}
},{}],3:[function(require,module,exports){
let calculate = require("./calculate.js");
let { Note, Subject, Group } = require("./classes.js");
let data = [];
let save = [];


$(document).ready(function () {
    let upload = $("#dataFile");
    let btnCalculate = $("#calculate");
    makeSchema(save);


    btnCalculate.click(function () {
        // Make sure a file was selected
        if (upload.files.length > 0) {
            let reader = new FileReader(); // File reader to read the file

            // This event listener will happen when the reader has read the file
            reader.addEventListener("load", function () {
                data = JSON.parse(reader.result); // Parse the result into an object
                doCalculations(data);
            });

            reader.readAsText(upload.files[0]); // Read the uploaded file
        }
    });

    $("#addFirstGroup").click(() => {
        let name = $('#createGroup-inputName').val();
        let coef = $('#createGroup-inputCoef').val();
        addGroup(name, coef);
    });


    function addGroup(name, coef) {
        group = new Group(name, coef, []);
        save.push(group);
        doCalculations(save);
    }

    function addSubject (groupName, name, coef) {
        groupIndex = save.findIndex(x => x.name === groupName);
        subject = new Subject(name, coef, []);
        save[groupIndex].subjects.push(subject);

        doCalculations(save);
    };

    function addNote (groupName, subjectName, name, coef, value) {
        groupIndex = save.findIndex(x => x.name === groupName);
        subjectIndex = save[groupIndex].subjects.findIndex(x => x.name === subjectName);
        note = new Note(name, coef, value);
        save[groupIndex].subjects[subjectIndex].notes.push(note);
        console.log(save);
        doCalculations(save);
    };
    
    function groupRow(name, coef)
    {
        let html = $(`<div id="${name}-group" class="group"></div>`);
        html.append(`
        <div class="row">
            <div class="col">
                <label class="name">${name}</label>
                <label class="coef">${coef}</label>
            </div>
            <hr>
        </div>
        `);
        return html;
    }

    function subjectRow(parent, name, coef)
    {
        let html = $(`<div id="${parent.name+"-"+name}-subject" class="subject"></div>`)
        html.append(`
        <div class="row">
            <div class="col">
                <label class="name">${name}</label>
                <label class="coef">${coef}</label>
                <div id="${parent.name+"-"+name}-notes"></div>
            </div>
            <hr>
        </div>
        `);
        return html;
    }

    function noteRow(parent, name, coef, value)
    {
        let html = $(`<div id="${parent.name+"-"+name}-note" class="note"></div>`)
        html.append(`
        <div class="row text-center">
            <div class="col-8 name">${name}</div>
            <div class="col-2 coef">${coef}</div>
            <div class="col-2 note">${value}</div>
            <hr>
        </div>
        `);
        return html;
    }

    function formRow(parent, btn, firstInput, secondInput="", thirdInput="")
    {
        //check id value
        let id = "";
        if(typeof parent === "string")
            id = parent;
        else
            id = parent.name;
            
        let html = $(`<div class="row"><div id="${id}-form" class="col"></div></div>`);
        let formRow = $(`<div id="${id}-formRow" class="form-inline ml-auto"></div>`)
        
        if(thirdInput != "")
            thirdInput = `<input type="number" class="form-control" id="${id}-inputNote" placeholder="${thirdInput}">`;


        let inputs = $(`
            <input type="text" class="form-control" id="${id}-inputName" placeholder="${firstInput}">
            <input type="number" class="form-control" id="${id}-inputCoef" placeholder="${secondInput}">
            ${thirdInput}
        `);

        inputs.append(thirdInput);

        formRow.append(inputs);
        formRow.append(btn);

        //make html
        html.append(formRow);

        return html;
    }

    function makeSchema(data) {

        let main = $('#main');
        main.empty();
        if(data != [])
        {
            data.forEach((group) => {
                if(group.subjects){

                    main.append(groupRow(group.name, group.coef));
                    group.subjects.forEach((subject) => {
                        $(`#${group.name}-group`).append(subjectRow(group, subject.name, subject.coef));
                        
                        if(subject.notes != [])
                        {
                            subject.notes.forEach((note) => {

                                //create note row
                                let id = `#${group.name}-${subject.name}-notes`;
                                console.log(note)
                                $(id).append(noteRow(subject, note.name, note.coef, note.value));                                
                            });
                        }


                        //create note form
                        let btnSubject = $(`<button id="${group.name}-${subject.name}-btn" class="btn btn-warning">Ajouter une note</button>`);
                        $(`#${group.name}-${subject.name}-subject`).append(formRow(subject, btnSubject, "Sujet", "Coef", "Note"));
                        $(document).off().on("click", `#${group.name}-${subject.name}-btn`,() => addNote(group.name, subject.name, $('#'+subject.name+"-inputName").val(), $('#'+subject.name+"-inputCoef").val(), $('#'+subject.name+"-inputNote").val()));
        
                    });
                }

                //create subject form
                let btnGroup = $(`<button id="${group.name}-btn" class="btn btn-primary">Ajouter une matière</button>`);
                $(`#${group.name}-group`).append(formRow(group, btnGroup, "Nom de la matière", "Coef"));
                main.off().on("click", `#${group.name}-btn`,() => addSubject(group.name, $('#'+group.name+"-inputName").val(),$('#'+group.name+"-inputCoef").val()));
            });
        }
    }


    function doCalculations(data) {
        makeSchema(data);
        console.log(data)
        let avg = calculate(data);
        $('#generalAverage').empty();
        $('#generalAverage').append(avg);

    }
});


},{"./calculate.js":1,"./classes.js":2}]},{},[3]);
