class Subject {
    name = "";
    coef = 0;
    notes = new Array();

    constructor(name, coef, notes) {
        this.name = name;
        this.coef = coef;
        this.notes = notes;
    }
}

class Group {
    name = "";
    coef = 0;
    subjects = new Array();

    constructor(name, coef, subjects) {
        this.name = name;
        this.coef = coef;
        this.subjects = subjects;
    }
}

class Note {
    name = "";
    coef = 0;
    value = 0;

    constructor(name, coef, value) {
        this.name = name;
        this.coef = coef;
        this.value = value;
    }
}

module.exports = {
    Subject: Subject,
    Group: Group,
    Note: Note,
}